import { z, ZodType } from "zod";

export class AddresstValidation {
    static readonly CREATE : ZodType = z.object({
        contact_id: z.number().positive(),
        country: z.string().min(1).max(100),
        province: z.string().min(1).max(100).optional(),
        city: z.string().min(1).max(100).optional(),
        street: z.string().min(1).max(255).optional(),
        postal_code: z.string().min(1).max(10)
    })

    static readonly UPDATE : ZodType = z.object({
        id: z.number().positive(),
        contact_id: z.number().positive(),
        country: z.string().min(1).max(100),
        province: z.string().min(1).max(100).optional(),
        city: z.string().min(1).max(100).optional(),
        street: z.string().min(1).max(255).optional(),
        postal_code: z.string().min(1).max(10)
    })


    static readonly GET : ZodType = z.object({
        contact_id: z.number().positive(),
        id: z.number().positive(),
    })

    static readonly REMOVE : ZodType = z.object({
        contact_id: z.number().positive(),
        id: z.number().positive(),
    })
}