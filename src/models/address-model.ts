import { Address, Contact } from "@prisma/client";

export type CreateAddressRequest = {
    contact_id: number;
    country: string;
    province?: string;
    city?: string;
    street?: string;
    postal_code: string;
}

export type UpdateAddressRequest = {
    id: number;
    contact_id: number;
    country: string;
    province?: string;
    city?: string;
    street?: string;
    postal_code: string;
}

export type GetAddressRequest = {
    contact_id: number;
    id: number
}

export type RemoveAddressRequest = GetAddressRequest

export type AddressResponse = {
    id: number;
    country: string;
    province?: string | null;
    city?: string | null;
    street?: string | null;
    postal_code: string;
}

export type SearchContactRequest = {
    name?: string;
    phone?: string;
    email?: string;
    page: number,
    size: number
}

export function toAddressResponse(address: Address): AddressResponse {
    return {
        id: address.id,
        country: address.country,
        province: address.province,
        city: address.city,
        street: address.street,
        postal_code: address.postal_code
    }
}
