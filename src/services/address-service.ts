import { Address, Contact, User } from "@prisma/client";
import { prismaClient } from "../applications/database";
import { ResponseError } from "../error/response-error";
import { CreateUserRequest, LoginUserRequest, UpdateUserRequest, UserResponse, toUserResponse } from "../models/user-model";
import { UserValidation } from "../validations/user-validation";
import { Validation } from "../validations/validation";
import bcrypt from "bcrypt"
import {v4 as uuid} from "uuid"
import { ContactResponse, CreateContactRequest, SearchContactRequest, UpdateContactRequest, toContactResponse } from "../models/contact-model";
import { ContactValidation } from "../validations/contact-validation";
import { Pageable } from "../models/page";
import { AddressResponse, CreateAddressRequest, GetAddressRequest, RemoveAddressRequest, UpdateAddressRequest, toAddressResponse } from "../models/address-model";
import { AddresstValidation } from "../validations/address-validation";
import { ContactService } from "./contact-service";
import { logger } from "../applications/logging";

export class AddressService {
    static async create(user: User, request: CreateAddressRequest) : Promise<AddressResponse> {
        const createRequest = Validation.validate(AddresstValidation.CREATE, request);
        await ContactService.checkContactMustExists(user.username, request.contact_id)

        const address = await prismaClient.address.create({
            data: createRequest
        })

        return toAddressResponse(address);
    }

    static async checkAddressMustExists(contactId: number, addressId: number): Promise<Address> {
        const address = await prismaClient.address.findFirst({
            where: {
                id: addressId,
                contact_id: contactId
            }
        })

        if (!address) {
            throw new ResponseError(404, "Address is not found")
        }

        return address;
    }

    static async get(user: User, request: GetAddressRequest): Promise<AddressResponse> {
        const getRequest = Validation.validate(AddresstValidation.GET, request)
        await ContactService.checkContactMustExists(user.username, request.contact_id)
        const address = await this.checkAddressMustExists(getRequest.contact_id, getRequest.id)

        return toAddressResponse(address);
    
    }

    static async update(user: User, request: UpdateAddressRequest): Promise<AddressResponse> {
        const updateRequest = Validation.validate(AddresstValidation.UPDATE, request);
        await ContactService.checkContactMustExists(user.username, request.contact_id)
        await this.checkAddressMustExists(updateRequest.contact_id, updateRequest.id)

        const address = await prismaClient.address.update({
            where: {
                id: updateRequest.id,
                contact_id: updateRequest.contact_id
            },
            data: updateRequest
        })

        return toAddressResponse(address);
    }

    static async remove(user: User, request: RemoveAddressRequest): Promise<AddressResponse> {
        const removeRequest = Validation.validate(AddresstValidation.REMOVE, request)
        await ContactService.checkContactMustExists(user.username, request.contact_id)
        await this.checkAddressMustExists(removeRequest.contact_id, removeRequest.id)

        const address = await prismaClient.address.delete({
            where: {
                id: removeRequest.id,
                contact_id: removeRequest.contact_id
            }
        })

        return toAddressResponse(address)
    }

    static async list(user: User, contactId: number): Promise<Array<AddressResponse>> {
        await ContactService.checkContactMustExists(user.username, contactId);

        const addresses = await prismaClient.address.findMany({
            where:{
                contact_id: contactId
            }
        });

        return addresses.map((address) => toAddressResponse(address));

    }
}